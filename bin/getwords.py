#!/usr/bin/env python
"""
Automatically generate a basic wordlist.tsv file.

This file is not necessary for the game to run; it's simply for convenience.
"""

# If you're not using a *NIX system, you'll have to provide your own list:
INPUT_FILE = r"/usr/share/dict/words" # or perhaps r"/usr/dict/words"

# Read the input file, which should be a list of UTF-8 words (one per line):
print("Reading input file...")
with open(INPUT_FILE, "r") as src:
	data = src.readlines();

# Output a wordlist.tsv file using Latin-1 encoding (it's what the game uses).
print("Writing output file...")
with open("wordlist.tsv", "wb") as out:
	seen = []
	for line in data:
		# Words are expected to be entirely in upper case, without spaces.
		word = line.decode("utf-8").upper().strip().encode("latin-1")
		# Only include UNIQUE words, between two and [n] letters long.
		if word not in seen and "'" not in word and 2 <= len(word) :# <= 6:
			seen.append(word)
			# Some words have special effects:
			if word in ["LIFE", "LIVES", "LIVING"]:
				score = len(word)
				lives = len(word) - 3
				flags = ""
			elif word in ["ZAP", "ZAPS", "ZAPPED", "ZAPPER"]:
				score = len(word)
				lives = 0
				flags = "SHOOT"
			elif word in ["KILL", "KILLS", "KILLED", "KILLER"]:
				score = len(word)
				lives = 0
				flags = "KILLALL"
			elif word in ["FUCK", "FUCKS", "FUCKED", "FUCKER", "FUCKING"]:
				score = 0
				lives = 0
				flags = "ENEMY"
			elif word in ["SHIT", "SHITS", "SHITTY", "SHITTING"]:
				score = -100 * len(word)
				lives = 0
				flags = ""
			elif word in ["CUNT", "CUNTS"]:
				score = -100 * len(word)
				lives = -1 * len(word)
				flags = "ENEMY"
			else:
				score = len(word)
				lives = 0
				flags = ""
			# Write this word to the output file.
			out.write("\t".join([word, str(score), str(lives), flags]) + "\n")

# Show a "finished" message.
print("Done!")

