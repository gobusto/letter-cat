/**
@file catPhrase.h

@brief Provides structures and functions for handling the "word list" lookup.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CAT_PHRASE_H__
#define __CAT_PHRASE_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Constants. */

#define PHRASE_FLAG_SHOOT   0x01  /**< @brief Grant the shooting power-up. */
#define PHRASE_FLAG_ENEMY   0x02  /**< @brief Spawn an extra enemy.        */
#define PHRASE_FLAG_KILLALL 0x04  /**< @brief Instantly kill all enemies.  */
#define PHRASE_FLAG_UNUSEDD 0x08  /**< @brief Unused; might be used later. */
#define PHRASE_FLAG_UNUSEDE 0x10  /**< @brief Unused; might be used later. */
#define PHRASE_FLAG_UNUSEDF 0x20  /**< @brief Unused; might be used later. */
#define PHRASE_FLAG_UNUSEDG 0x40  /**< @brief Unused; might be used later. */
#define PHRASE_FLAG_UNUSEDH 0x80  /**< @brief Unused; might be used later. */

/**
@brief about

writeme
*/

typedef struct
{

  unsigned char flags;  /**< @brief about */

  int lives;  /**< @brief about */
  int score;  /**< @brief about */

} phrase_s;

/**
@brief about

writeme

@param file_name about
@return writeme
*/

int phraseInit(const char *file_name);

/**
@brief about

writeme
*/

void phraseQuit(void);

/**
@brief about

writeme

@param text about
@return writeme
*/

const phrase_s *phraseFind(const char *text);

#ifdef __cplusplus
}
#endif

#endif /* __CAT_PHRASE_H__ */
