/**
@file wordtree.h

@brief Provides an "associative array" structure designed for fast look-up.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __WORDTREE_H__
#define __WORDTREE_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief This structure describes a single "node" within a "tree" of nodes.

Each node structure represents a single 8-bit byte. A byte value of zero means
that THIS is the required node, in which case the "data" attribute is returned.
For other byte values (1-255), a "branch" node (sub-tree) is checked instead.
*/

typedef struct _wordtree_s
{

  struct _wordtree_s *parent; /**< @brief Pointer to the parent node. */

  void *data; /**< @brief User-defined data pointer. */

  unsigned char offset; /**< @brief The lowest byte index in tree->branch[] */
  unsigned char length; /**< @brief The number of indices in tree->branch[] */

  struct _wordtree_s **branch;  /**< @brief A list of "child" tree nodes. */

} wordtree_s;

/**
@brief Create a new "tree" node structure.

Users can call this to create a new "root" node. Internally, this function is
also used to create "branch" nodes.

@return A new tree structure on success, or NULL on failure.
*/

wordtree_s *treeCreate(void);

/**
@brief Delete an existing tree structure, along with any sub-trees.

@param tree The root node of the tree to be deleted.
@param func Optional user-supplied callback function for each item deleted.
@param user An optional user-data pointer passed to the callback for each item.
@return Always returns NULL.
*/

wordtree_s *treeDelete(wordtree_s *tree, void *(*func)(const char*, void*, void*), void *user);

/**
@brief Iterate over all entries in a wordtree structure.

@param tree The root node of the tree.
@param func Optional user-supplied callback function for each item found.
@param user An optional user-data pointer passed to the callback for each item.
*/

void treeEachItem(wordtree_s *tree, void *(*func)(const char*, void*, void*), void *user);

/**
@brief Insert a new item into a tree, replacing the old value if necessary.

@param tree The root node of the tree.
@param key The string of text used to store/retrieve the data.
@param data The data to be stored; may be NULL.
@param old_data An (optional) void pointer, used to pass back any "old" data.
@return Zero (false) on failure, or one (true) on success.
*/

int treeInsert(wordtree_s *tree, const char *key, void *data, void **old_data);

/**
@brief Get a given (named) item from a tree structure.

@param tree The root node of the tree.
@param key The string of text used to store/retrieve the data.
@return The data stored for the specified key on success, or NULL on failure.
*/

void *treeSearch(wordtree_s *tree, const char *key);

#ifdef __cplusplus
}
#endif

#endif  /* __WORDTREE_H__ */
