/**
@file chain.h

@brief Provides structures and functions for double-ended, doubly-linked lists.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __V2D_CHAIN_H__
#define __V2D_CHAIN_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief This structure represents a single item within a doubly-linked list.

This will generally be used as a sub-structure within a larger one, with the
main structure accessing the link via something->link and the link accessing
the main structure via the link->data attribute.
*/

typedef struct _v2d_link_s
{

  struct _v2d_chain_s *chain; /**< @brief The chain to which this belongs. */
  void                *data;  /**< @brief Used to store user-defined data. */
  struct _v2d_link_s  *prev;  /**< @brief The previous link in the chain.  */
  struct _v2d_link_s  *next;  /**< @brief The next link in the chain.      */

} v2d_link_s;

/**
@brief This structure is used as a "container" for linked-list data items.

For example, a "Library" could be represented as a chain structure, with each
link in the chain referencing a "Book" structure via the link->data attribute.

As with the v2d_link_s structure, a user_data pointer is provided so that the
chain can reference the structure that contains it. Additionally, a user_type
value is provided so that a program using multiple chains for different kinds
of data is able to check which type of data the chain is being used to store.
*/

typedef struct _v2d_chain_s
{

  long  user_type;  /**< @brief Not used internally; provided for the user. */
  void *user_data;  /**< @brief Not used internally; provided for the user. */

  v2d_link_s *first;  /**< @brief Points to the first item within the list. */
  v2d_link_s *last;   /**< @brief Points to the final item within the list. */

} v2d_chain_s;

/**
@brief Get the user-data pointer of the next item in a chain.

This makes it easy to loop through lists: a = (user_s*)v2dLinkGetNext(a->link)

@param p A pointer to a v2d_link_s structure.
@return The user-data of the next item in the list, or NULL if none exists.
*/

#define v2dLinkGetNext(p) ((p) ? ((p)->next ? (p)->next->data : 0) : 0)

/**
@brief Get the user-data pointer of the previous item in a chain.

This makes it easy to loop through lists: a = (user_s*)v2dLinkGetPrev(a->link)

@param p A pointer to a v2d_link_s structure.
@return The user-data of the previous item in the list, or NULL if none exists.
*/

#define v2dLinkGetPrev(p) ((p) ? ((p)->prev ? (p)->prev->data : 0) : 0)

/**
@brief Create a new (empty) chain.

@param user_type A user-defined integer value.
@param user_data A user-defined pointer value.
@return A new chain structure on success, or NULL on failure.
*/

v2d_chain_s *v2dChainCreate(long user_type, void *user_data);

/**
@brief Free a previously-created chain structure and any links it contains.

NOTE: Any user-data referenced by the link->data pointers will NOT be freed!

@param chain The chain to be deleted.
@return The value of the "user_data" pointer, or NULL if "chain" is NULL.
*/

void *v2dChainDelete(v2d_chain_s *chain);

/**
@brief Create a new link structure.

@param data The pointer to store in the link->data attribute.
@return A new link structure on success, or NULL on failure.
*/

v2d_link_s *v2dLinkCreate(void *data);

/**
@brief Free a previously-created link structure.

The chain to which the link belongs (and the next/previous list items) will be
automatically updated so that they no longer reference the deleted item.

@param link The link to be deleted.
@return The value of the "data" pointer, or NULL if "chain" is NULL.
*/

void *v2dLinkDelete(v2d_link_s *link);

/**
@brief Remove a link structure from the chain/links it is associated with.

This does not actually delete the link structure; it simply isolates it.

@param link The link to be unlinked.
*/

void v2dLinkUnlink(v2d_link_s *link);

/**
@brief Add a link to a chain.

Note that the link is automatically unlinked from any other chain/links first.

@param chain The chain that the link should be added to.
@param prev The previous item in the chain. NULL means "insert as first item".
@param link The link to be added to the chain.
@return Zero = success, negative = NULL pointer, positive = prev is invalid.
*/

int v2dLinkInsert(v2d_chain_s *chain, v2d_link_s *prev, v2d_link_s *link);

/**
@brief Move a link along a chain (either forwards or backwards) by one place.

This is essentially just a convenient wraper for the v2dLinkInsert() function.

@param link The link to be moved.
@param fwd The link is moved forwards if this is true, or backwards if not.
@return Zero = success, negative = NULL pointer, positive = prev is invalid.
*/

int v2dLinkMove(v2d_link_s *link, int fwd);

#ifdef __cplusplus
}
#endif

#endif  /* __V2D_CHAIN_H__ */
