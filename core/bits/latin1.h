/**
@file latin1.h

@brief Provides support for handling Latin-1 (ISO-8859-1) character encodings.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_LATIN1_H__
#define __QQQ_LATIN1_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Determine if a Latin-1 character is upper-case or not.

This is similar to the isupper() function provided by ctype.h

@param c An unsigned, 8-bit character value.
@return True if the value is an upper-case Latin-1 character, or False if not.
*/

#define latin1_isupper(c) (((c) >= 0x41 && (c) <= 0x5A) || ((c) >= 0xC0 && (c) <= 0xDF))

/**
@brief Determine if a Latin-1 character is lower-case or not.

This is similar to the islower() function provided by ctype.h

NOTE: The ((c) <= 0xFF) check has been omitted in order to prevent GCC warnings
about "always true due to limited range of data-type", so be careful when using
integers with a larger range.

@param c An unsigned, 8-bit character value.
@return True if the value is a lower-case Latin-1 character, or False if not.
*/

#define latin1_islower(c) (((c) >= 0x61 && (c) <= 0x7A) || ((c) >= 0xE0))

/**
@brief Determine if a Latin-1 character is an alphabetic character or not.

This is similar to the isupper() function provided by ctype.h

@param c An unsigned, 8-bit character value.
@return True if the value is an alphabetic Latin-1 character, or False if not.
*/

#define latin1_isalpha(c) (latin1_isupper(c) || latin1_islower(c))

/**
@brief Convert a lower-case Latin-1 character into its upper-case equivalent.

This is similar to the toupper() function provided by ctype.h

@param c An unsigned, 8-bit character value.
@return The upper-case variant of the character, or the original value on error.
*/

#define latin1_toupper(c) (latin1_islower(c) ? (c)-32 : (c))

/**
@brief Convert an upper-case Latin-1 character into its lower-case equivalent.

This is similar to the tolower() function provided by ctype.h

@param c An unsigned, 8-bit character value.
@return The lower-case variant of the character, or the original value on error.
*/

#define latin1_tolower(c) (latin1_isupper(c) ? (c)+32 : (c))

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_LATIN1_H__ */
