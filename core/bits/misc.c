/**
@file misc.c

@brief Various bits and pieces that don't really belong anywhere else.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "misc.h"

/*
[PUBLIC] Read a line of text from a file opened for reading.
*/

char *cat_readline(FILE *src)
{

  char *line = NULL;
  long start = 0;
  int pass = 0;

  for (start = ftell(src), pass = 0; pass < 2; ++pass)
  {

    long len, c;

    for (len = 0; (c = fgetc(src)); ++len)
    {
      if (c == EOF || c == '\n' || c == '\r') { break; }
      if (line) { line[len] = c; }
    }

    if (c == EOF && len == 0) { return NULL; }

    if (pass == 0)
    {
      line = (char*)malloc(len+1);
      if (!line) { return NULL; }
      memset(line, 0, len+1);
      fseek(src, start, SEEK_SET);
    }

  }

  return line;

}
