/**
@file catAudio.h

@brief Handles sound effects and music.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CAT_AUDIO_H__
#define __CAT_AUDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Enumerates possible sound effects.

These are passed to the sndPlayEffect() function.
*/

typedef enum
{

  CAT_SOUND_JUMP, /**< @brief Player jump sound. */
  CAT_SOUND_FIRE, /**< @brief Player shoot sound. */
  CAT_SOUND_GRAB, /**< @brief Letter-tile "grab" sound. */
  CAT_SOUND_DROP, /**< @brief Letter-tile "drop" sound. */
  CAT_SOUND_OPEN, /**< @brief Exit door open sound. */
  CAT_SOUND_MEOW, /**< @brief The "don't-have-the-shoot-power-up" sound. */
  CAT_SOUND_BONUS /**< @brief Played when the "shoot" power-up is granted. */

} cat_sound_t;

/**
@brief Initialise the audio subsystem.

Call this function before doing anything else.

@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int sndInit(void);

/**
@brief Shut down the audio subsystem.

Call this function when the program shuts down.
*/

void sndQuit(void);

/**
@brief Play a sound effect.

@param id The type of sound to play.
*/

void sndPlayEffect(cat_sound_t id);

/**
@brief Play (and optionally loop) a music file.

Any currently-playing music is stopped first.

@param file_name The name (and path) of the music file to play.
@return Zero on success or non-zero on error. (TODO: Use an ERR_ABC enum?)
*/

int sndPlayMusic(const char *file_name);

/**
@brief Stop any currently-playing music.

Has no effect if no music is currently playing.
*/

void sndStopMusic(void);

#ifdef __cplusplus
}
#endif

#endif /* __CAT_AUDIO_H__ */
