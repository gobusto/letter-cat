/**
@file bitmap.h

@brief Provides facilities for loading 2D pixel-based image files into memory.

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __QQQ_BITMAP_H__
#define __QQQ_BITMAP_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Datatypes. */

typedef unsigned long bmp_size_t; /**< @brief Image width/height values.   */
typedef unsigned char bmp_bpp_t;  /**< @brief Image bits-per-pixel values. */
typedef unsigned char bmp_byte_t; /**< @brief Image pixel data values.     */

/**
@brief The main bitmap image data structure.

Bitmaps are stored in LLL, LALALA, RGBRGBRGB, or RGBARGBARGBA format, depending
on the bits-per-pixel depth (8/16/24/32), where L = Luminance (greys), R = Red,
G = Green, B = Blue and A = Alpha (opacity). Any 16-bits-per-channel images are
quantised down to 8-bits-per-channel format for consistency of storage.
*/

typedef struct
{

  bmp_size_t w;     /**< @brief The image width in pixels.  */
  bmp_size_t h;     /**< @brief The image height in pixels. */
  bmp_bpp_t bpp;    /**< @brief The image depth in bits: 8, 16, 24, or 32.   */
  bmp_byte_t *data; /**< @brief An array of pixel data: Width*Height*(BPP/8) */

} bmp_image_s;

/**
@brief Initialise everything. Call this when your program starts up.

Internally, this is redirected to whatever is required by the API used.
For example, a DevIL back-end would call ilInit() here.

@return Zero on success, or non-zero on failure.
*/

int bmpInit(void);

/**
@brief Shut everything down. Call this when your program finishes.

Internally, this is redirected to whatever is required by the API used.
*/

void bmpQuit(void);

/**
@brief Load an image from a file.

The format of the image may or may not be supported, depending on the back-end
being used. For example, DevIL supports Unreal ".utx" texture files, but other
APIs might not.

@param file_name The name of the file to be loaded.
@return A pointer to the new image structure on success, or NULL on failure.
*/

bmp_image_s *bmpLoadFile(const char *file_name);

/**
@brief Create a new image data structure.

@param w The width of the image to be created.
@param h The height of the image to be created.
@param bpp The BPP of the image to be created.
@return A pointer to the new image structure on success, or NULL on failure.
*/

bmp_image_s *bmpCreate(bmp_size_t w, bmp_size_t h, bmp_bpp_t bpp);

/**
@brief Delete a previously created image structure.

@param image The image to be freed.
*/

void bmpFree(bmp_image_s *image);

#ifdef __cplusplus
}
#endif

#endif /* __QQQ_BITMAP_H__ */
