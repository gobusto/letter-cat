/**
@file bitmap_internal.c

@brief Provides a self-sufficient implementation of the functions in bitmap.h

Copyright (C) 2013-2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

/*
[PUBLIC] Initialise everything. Call this when your program starts up.
*/

int bmpInit(void) { return 0; }

/*
[PUBLIC] Shut everything down. Call this when your program finishes.
*/

void bmpQuit(void) { }

/**
@brief [INTERNAL] Load a TGA image from a file.

@todo Support compressed TGA.

@param len The length of the "data" array.
@param data A pointer to a raw data buffer, read in from a file.
@return A pointer to the new image structure on success, or NULL on failure.
*/

static bmp_image_s *bmpLoadTGA(long len, const unsigned char *data)
{

  bmp_image_s *bmp = NULL;

  bmp_size_t w = 0;
  bmp_size_t h = 0;

  /* Ensure that the buffer is valid. */

  if (len < 18 || !data) { return NULL; }

  /* Ensure that the buffer is an unmapped, uncompressed Targa RGB image. */

  if (data[1] != 0) { return NULL; }

  if (data[2] != 2 && data[2] != 3) { return NULL; }

  /* Ensure that the buffer is long enough. */

  w = data[12] + (data[13]*256);
  h = data[14] + (data[15]*256);

  if ((long)(18 + data[0] + (w*h*(data[16]/8))) > len) { return NULL; }

  /* Only support 8-bit, 24-bit or 32-bit images. */

  if (data[16] != 8 && data[16] != 24 && data[16] != 32) { return NULL; }

  /* Allocate memory */

  bmp = bmpCreate(w, h, data[16]);

  if (!bmp) { return NULL; }

  /* Read data. */

  for (h = 0; h < bmp->h; h++)
  {

    for (w = 0; w < bmp->w; w++)
    {

      const long i = (h * bmp->w * (bmp->bpp/8)) + (w * (bmp->bpp/8));

      if (bmp->bpp != 8)
      {

        bmp->data[i+0] = data[18+data[0]+i+2];
        bmp->data[i+1] = data[18+data[0]+i+1];
        bmp->data[i+2] = data[18+data[0]+i+0];

        if (bmp->bpp == 32) { bmp->data[i+3] = data[18+data[0]+i+3]; }

      } else { bmp->data[i] = data[18+data[0]+i]; }

    }

  }

  /* Return result (success). */

  return bmp;

}

/*
[PUBLIC] Load an image from a file.
*/

bmp_image_s *bmpLoadFile(const char *file_name)
{

  bmp_image_s *bmp = NULL;
  FILE          *fsrc = NULL;
  unsigned char *data = NULL;
  long          size  = 0;

  /* Attempt to open the requested file. */

  if (!file_name) { return NULL; }
  fsrc = fopen(file_name, "rb");
  if (!fsrc) { return NULL; }

  /* Get the length of the file in bytes. */

  fseek(fsrc, 0, SEEK_END);
  size = (long)ftell(fsrc);
  rewind(fsrc);

  /* Copy the data into memory (with an extra zero byte, in case it's text). */

  data = (unsigned char*)malloc(size + 1);

  if (data)
  {
    memset(data, 0, size + 1);
    fread(data, 1, size, fsrc);
  }

  fclose(fsrc);

  if (!data) { return NULL; }

  /* Try to convert the data into an image. */

  bmp = bmpLoadTGA(size, data);

  /* Free the data buffer and return the result. */

  free(data);
  return bmp;

}
