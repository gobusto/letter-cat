/**
@file catGraphics_GL1.c

@brief An OpenGL 1.x implementation of the functions defined in catGraphics.h

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
#else
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "catGraphics.h"
#include "catWorld.h"
#include "bitmap/bitmap.h"
#include "bits/latin1.h"

/**
@brief [INTERNAL] This determines which way up the HUD icons are drawn.

Usually, the target word is at the top of the screen and everything else is at
the bottom. However, a touch-screen tablet version of the game would need room
for "virtual buttons" at the bottom-left and bottom-right edges, so having the
target word at the BOTTOM (and everything else at the top) makes more sense.

Set this to zero to draw the status bar at the bottom of the screen (with the
target word at the top) or non-zero to draw the status bar at the top (with the
target word at the bottom).

@todo Allow this setting to be loaded from a config file at some point?
*/

#define GFX_INVERT_HUD 0

/**
@brief [INTERNAL] Get the X position of a Latin-1 character on a 16x16 grid.

This is used to find the position of a specific glyph within the "font" image.

@param c A Latin-1 character.
@return The X position of that character on a 16x16 grid, from 0.0 to 1.0.
*/

#define gfxGlyphX(c) (((unsigned char)(c) % 16) / 16.0)

/**
@brief [INTERNAL] Get the Y position of a Latin-1 character on a 16x16 grid.

This is used to find the position of a specific glyph within the "font" image.

@param c A Latin-1 character.
@return The Y position of that character on a 16x16 grid, from 0.0 to 1.0.
*/

#define gfxGlyphY(c) (1 - ((1 + ((unsigned char)(c) / 16)) / 16.0))

/* Global objects and variables. */

GLfloat gfx_width  = 640; /**< @brief The display width in pixels. */
GLfloat gfx_height = 480; /**< @brief The display height in pixels. */

GLfloat gfx_backdrop_w = 0; /**< @brief Width of the backdrop in pixels. */
GLfloat gfx_backdrop_h = 0; /**< @brief Height of the backdrop in pixels. */
GLuint  gfx_backdrop   = 0; /**< @brief The backdrop texture. */

GLuint gfx_sprite_wall   = 0; /**< @brief The generic "wall" image texture.  */
GLuint gfx_sprite_player = 0; /**< @brief The player character sprite sheet. */
GLuint gfx_sprite_bullet = 0; /**< @brief The bullet image sprite sheet.     */
GLuint gfx_sprite_enemy  = 0; /**< @brief The "walking" enemy sprite sheet.  */
GLuint gfx_sprite_exit   = 0; /**< @brief The level exit sprite sheet.       */
GLuint gfx_sprite_tile   = 0; /**< @brief The background image for letters.  */
GLuint gfx_sprite_font   = 0; /**< @brief A "Latin-1" font as a 16x16 grid.  */

GLuint gfx_world_dlist = 0; /**< @brief A display list for the "wall" tiles. */

/**
@brief [INTERNAL] Load an image file as an OpenGL texture.

@param file_name The filename to be loaded.
@param w An (optional) output variable for storing the image width in pixels.
@param h An (optional) output variable for storing the image height in pixels.
@return A valid GLuint texture ID on success or zero on failure.
*/

static GLuint gfxLoadTexture(const char *file_name, GLfloat *w, GLfloat *h)
{

  bmp_image_s *image = NULL;
  GLuint gl_texture = 0;
  GLenum format;

  /* Attempt to load the image file. */

  image = bmpLoadFile(file_name);

  if (!image)
  {
    if (w) { *w = 0; }
    if (h) { *h = 0; }
    fprintf(stderr, "[WARNING] Could not load image: '%s'\n", file_name ? file_name : NULL);
    return 0;
  }

  if (w) { *w = image->w; }
  if (h) { *h = image->h; }

  switch (image->bpp)
  {
    case 32: format = GL_RGBA;            break;    /* RGB with alpha.       */
    case 24: format = GL_RGB;             break;    /* RGB only.             */
    case 16: format = GL_LUMINANCE_ALPHA; break;    /* Greyscale with alpha. */
    case 8:  format = GL_LUMINANCE;       break;    /* Greyscale only.       */
    default: bmpFree(image);              return 0; /* Unknown pixel format. */
  }

  /* Create an OpenGL texture and copy the pixel data into it. */

  glGenTextures(1, &gl_texture);
  glBindTexture(GL_TEXTURE_2D, gl_texture);

  gluBuild2DMipmaps(GL_TEXTURE_2D, image->bpp / 8, image->w, image->h, format, GL_UNSIGNED_BYTE, image->data);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  /* Free the bitmap data structure and return the OpenGL texture. */

  bmpFree(image);
  return gl_texture;

}

/*
[PUBLIC] Initialise the graphics sub-system.
*/

int gfxInit(int w, int h)
{

  gfxQuit();

  if (w < 1 || h < 1) { return -1; }

  gfx_width = w;
  gfx_height = h;

  /* Output some debug info. */

  fprintf(stderr, "[OPENGL] Renderer: %s\n", glGetString(GL_RENDERER));
  fprintf(stderr, "[OPENGL] Version: %s\n", glGetString(GL_VERSION));
  fprintf(stderr, "[OPENGL] Vendor: %s\n", glGetString(GL_VENDOR));

  /* Set up the transformation matrix. */

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glScalef(64.0 / gfx_width, -64.0 / gfx_height, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  /* Set up OpenGL properties. */

  glEnable(GL_TEXTURE_2D);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glAlphaFunc(GL_GREATER, 0);
  glEnable(GL_ALPHA_TEST);

  /* Load media. */

  if (bmpInit() != 0) { return -1; }

  gfx_sprite_wall   = gfxLoadTexture("images/wall.tga", NULL, NULL);
  gfx_sprite_player = gfxLoadTexture("images/player.tga", NULL, NULL);
  gfx_sprite_bullet = gfxLoadTexture("images/bullet.tga", NULL, NULL);
  gfx_sprite_enemy  = gfxLoadTexture("images/enemy.tga", NULL, NULL);
  gfx_sprite_exit  = gfxLoadTexture("images/exit.tga", NULL, NULL);
  gfx_sprite_tile  = gfxLoadTexture("images/tile.tga", NULL, NULL);
  gfx_sprite_font  = gfxLoadTexture("images/font.tga", NULL, NULL);

  return 0;

}

/*
[PUBLIC] Shut down the graphics sub-system and free any allocated memory.
*/

void gfxQuit(void)
{

  gfxCleanupLevel();

  glDeleteTextures(1, &gfx_sprite_wall);
  glDeleteTextures(1, &gfx_sprite_player);
  glDeleteTextures(1, &gfx_sprite_bullet);
  glDeleteTextures(1, &gfx_sprite_enemy);
  glDeleteTextures(1, &gfx_sprite_exit);
  glDeleteTextures(1, &gfx_sprite_tile);
  glDeleteTextures(1, &gfx_sprite_font);

  bmpQuit();

}

/*
[PUBLIC] Prepare any per-level graphical stuff when a new map is loaded.
*/

void gfxPrepareLevel(const char *backdrop)
{

  const unsigned char gfx_ega[16][3] = {
    { 0x00, 0x00, 0x00 },
    { 0x00, 0x00, 0xAA },
    { 0x00, 0xAA, 0x00 },
    { 0x00, 0xAA, 0xAA },
    { 0xAA, 0x00, 0x00 },
    { 0xAA, 0x00, 0xAA },
    { 0xAA, 0x55, 0x00 },
    { 0xAA, 0xAA, 0xAA },
    { 0x55, 0x55, 0x55 },
    { 0x55, 0x55, 0xFF },
    { 0x55, 0xFF, 0x55 },
    { 0x55, 0xFF, 0xFF },
    { 0xFF, 0x55, 0x55 },
    { 0xFF, 0x55, 0xFF },
    { 0xFF, 0xFF, 0x55 },
    { 0xFF, 0xFF, 0xFF }
  };

  int id, x, y;

  /* Free any old data first. */

  gfxCleanupLevel();

  /* Load the backdrop image. */

  gfx_backdrop = gfxLoadTexture(backdrop, &gfx_backdrop_w, &gfx_backdrop_h);

  glBindTexture(GL_TEXTURE_2D, gfx_backdrop);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  if (gfx_backdrop)
  {
    #ifdef NDEBUG
      fprintf(stderr, "[DEBUG] Using backdrop image '%s' (%dx%d).\n", backdrop,
          (int)gfx_backdrop_w, (int)gfx_backdrop_h);
    #endif
  }

  /* Generate a display list for all of the wall tiles within the world. */

  #ifdef NDEBUG
    fprintf(stderr, "[DEBUG] Generating display list...\n");
  #endif

  gfx_world_dlist = glGenLists(1);
  glNewList(gfx_world_dlist, GL_COMPILE);

  glBindTexture(GL_TEXTURE_2D, gfx_sprite_wall);
  glBegin(GL_QUADS);

  for (y = 0; y < worldGetHeight(); y++)
  {
    for (x = 0; x < worldGetWidth(); x++)
    {
      id = worldGetTile(x, y) % 16;
      if (id)
      {
        glColor3ub(gfx_ega[id][0], gfx_ega[id][1], gfx_ega[id][2]);
        glTexCoord2f(0, 1); glVertex2f(x-0.5, y-0.5);
        glTexCoord2f(1, 1); glVertex2f(x+0.5, y-0.5);
        glTexCoord2f(1, 0); glVertex2f(x+0.5, y+0.5);
        glTexCoord2f(0, 0); glVertex2f(x-0.5, y+0.5);
      }
    }
  }

  glEnd();

  glEndList();

}

/*
[PUBLIC] Clean up any per-level graphical stuff.
*/

void gfxCleanupLevel(void)
{

  /* Free the old backdrop image (if one exists). */

  if (gfx_backdrop)
  {

    #ifdef NDEBUG
      fprintf(stderr, "[DEBUG] Freeing old background image...\n");
    #endif

    glDeleteTextures(1, &gfx_backdrop);

  }

  /* Free the old display list (if one exists). */

  if (gfx_world_dlist)
  {

    #ifdef NDEBUG
      fprintf(stderr, "[DEBUG] Freeing old display list...\n");
    #endif

    glDeleteLists(gfx_world_dlist, 1);
    gfx_world_dlist = 0;

  }

}

/**
@brief [INTERNAL] Draw a string of Latin-1 text.

@param x The X position of the first character.
@param y The Y position of the first character.
@param text The text to be drawn.
*/

static void gfxDrawText(float x, float y, const char *text)
{

  const float z = 1.0/16.0;

  long i, len;

  if (!text) { return; }

  len = strlen(text);

  glBindTexture(GL_TEXTURE_2D, gfx_sprite_font);

  glBegin(GL_QUADS);

  for (i = 0; i < len; i++)
  {

    float u = gfxGlyphX(text[i]);
    float v = gfxGlyphY(text[i]);

    glTexCoord2f(u+0, v+z); glVertex2f(i+x - 0.5, y - 0.5);
    glTexCoord2f(u+z, v+z); glVertex2f(i+x + 0.5, y - 0.5);
    glTexCoord2f(u+z, v+0); glVertex2f(i+x + 0.5, y + 0.5);
    glTexCoord2f(u+0, v+0); glVertex2f(i+x - 0.5, y + 0.5);

  }

  glEnd();

}

/**
@brief [INTERNAL] Draw a row of "tile" graphics.

This is used to draw tiles behind the target word and letter slots on the HUD.

@param n The number of tiles to draw.
@param x The X position of the first tile.
@param y The Y position of the first tile.
*/

static void gfxDrawTile(int n, float x, float y)
{

  glBindTexture(GL_TEXTURE_2D, gfx_sprite_tile);

  glBegin(GL_QUADS);
  glTexCoord2f(0, 1); glVertex2f(x  -0.5, y-0.5);
  glTexCoord2f(n, 1); glVertex2f(x+n-0.5, y-0.5);
  glTexCoord2f(n, 0); glVertex2f(x+n-0.5, y+0.5);
  glTexCoord2f(0, 0); glVertex2f(x  -0.5, y+0.5);
  glEnd();

}

/**
@brief [INTERNAL] Draw an actor.

NULL pointers are represented by a single (empty) tile, so that any empty slots
in the player's letter tray are handled correctly.

@param actor The actor to be drawn.
@param px The X position of the actor (so that it can be re-drawn on the HUD).
@param py The Y position of the actor (so that it can be re-drawn on the HUD).
@param for_hud If true, draw any letters in their original colors (not gray).
*/

static void gfxDrawActor(actor_s *actor, float px, float py, GLboolean for_hud)
{

  GLuint texture = 0;
  GLfloat x = 0;
  GLfloat y = 0;
  GLfloat w = 0;
  GLfloat h = 0;

  if (!actor)
  {
    glColor3ub(127, 127, 127);
    gfxDrawTile(1, px, py);
    return;
  }

  switch (actor->role)
  {

    case CAT_ROLE_PLAYER:

      texture = gfx_sprite_player;

      if      (actor->flags & CAT_FLAG_DAMAGED ) { x = 0.5; y = 0.0; }
      else if (actor->flags & CAT_FLAG_FIREHELD) { x = 0.0; y = 0.0; }
      else if (actor->timer >= 1.0             ) { x = 0.5; y = 0.5; }
      else                                       { x = 0.0; y = 0.5; }

    break;

    case CAT_ROLE_ENEMY:

      texture = gfx_sprite_enemy;

      if      (actor->flags & CAT_FLAG_DAMAGED      ) { x = 0.5; y = 0.0; }
      else if (actor->timer >= 2 && actor->timer < 3) { x = 0.0; y = 0.0; }
      else if (                     actor->timer < 1) { x = 0.0; y = 0.5; }
      else                                            { x = 0.5; y = 0.5; }

    break;

    case CAT_ROLE_BULLET:

      texture = gfx_sprite_bullet;

      if      (actor->timer >= 3) { x = 0.5; y = 0.0; }
      else if (actor->timer >= 2) { x = 0.0; y = 0.0; }
      else if (actor->timer >= 1) { x = 0.5; y = 0.5; }
      else                        { x = 0.0; y = 0.5; }

    break;

    case CAT_ROLE_EXIT:

      texture = gfx_sprite_exit;

      if      (actor->timer >= 3) { x = 0.5; y = 0.0; }
      else if (actor->timer >= 2) { x = 0.0; y = 0.0; }
      else if (actor->timer >= 1) { x = 0.5; y = 0.5; }
      else                        { x = 0.0; y = 0.5; }

    break;

    case CAT_ROLE_LETTER:

      if      (actor->timer > 0 && !for_hud) { glColor3ub(127, 127, 127); }
      else if (latin1_isupper(actor->flags)) { glColor3ub(255, 000, 000); }
      else                                   { glColor3ub(000, 127, 255); }

      gfxDrawTile(1, px, py);

      texture = gfx_sprite_font;

      x = gfxGlyphX(latin1_toupper(actor->flags));
      y = gfxGlyphY(latin1_toupper(actor->flags));

    break;

    default: texture = 0; break;

  }

  /* Set the sprite width/height and mirror it if necessary. */

  if (actor->role == CAT_ROLE_LETTER)
  {
    w = 1/16.0;
    h = 1/16.0;
  }
  else
  {

    w = 0.5;
    h = 0.5;

    if (actor->flags & CAT_FLAG_MIRROR)
    {
      x += w;
      w *= -1;
    }

  }

  /* Draw the sprite. */

  glBindTexture(GL_TEXTURE_2D, texture);
  glColor3ub(255, 255, 255);

  glBegin(GL_QUADS);
  glTexCoord2f(x+0, y+h); glVertex2f(px-0.5, py-0.5);
  glTexCoord2f(x+w, y+h); glVertex2f(px+0.5, py-0.5);
  glTexCoord2f(x+w, y+0); glVertex2f(px+0.5, py+0.5);
  glTexCoord2f(x+0, y+0); glVertex2f(px-0.5, py+0.5);
  glEnd();

}

/*
[PUBLIC] Draw everything in the game world.
*/

void gfxDrawGame(void)
{

  actor_s *player = NULL;
  actor_s *actor = NULL;

  glLoadIdentity();

  /* Get the player actor. */

  for (actor = worldGetActor(); actor; actor = actorGetNext(actor))
  {
    if (actor->role == CAT_ROLE_PLAYER) { player = actor; break; }
  }

  /* Draw a background. */

  {

    float xo = 0; /* Texture X offset. */
    float xs = 1; /* Texture X scale.  */

    if (gfx_backdrop)
    {

      glColor3ub(255, 255, 255);

      xo = (player ? player->pos[0]/25 : 0) / (gfx_backdrop_w / gfx_backdrop_h);
      xs = (gfx_width / gfx_height        ) * (gfx_backdrop_h / gfx_backdrop_w);

    } else { glColor3ub(0, 0, 0); }

    glBindTexture(GL_TEXTURE_2D, gfx_backdrop);

    glBegin(GL_QUADS);
    glTexCoord2f(xo,      0); glVertex2f(gfx_width / -64.0, gfx_height / +64.0);
    glTexCoord2f(xo + xs, 0); glVertex2f(gfx_width / +64.0, gfx_height / +64.0);
    glTexCoord2f(xo + xs, 1); glVertex2f(gfx_width / +64.0, gfx_height / -64.0);
    glTexCoord2f(xo,      1); glVertex2f(gfx_width / -64.0, gfx_height / -64.0);
    glEnd();

  }

  glPushMatrix();
  if (player) { glTranslatef(-player->pos[0], -player->pos[1], 0); }

  /* Draw grid. */

  glCallList(gfx_world_dlist);

  /* Draw actors. */

  for (actor = worldGetActor(); actor; actor = actorGetNext(actor))
  { gfxDrawActor(actor, actor->pos[0], actor->pos[1], GL_FALSE); }

  glPopMatrix();

  /* Draw HUD. */

  {

    const float x = 0.5 - (worldWordSize() / 2.0);
    #if GFX_INVERT_HUD
    const float y = (gfx_height / -64.0) + 1;
    #else
    const float y = (gfx_height / +64.0) - 1;
    #endif

    char buffer[512];

    long i = 0;

    /* Draw the "target" word at the top of the screen. */

    glColor3ub(222, 222, 99);
    gfxDrawTile(worldWordSize(), x, -y);
    glColor3ub(255, 255, 255);
    gfxDrawText(x, -y, worldGetWord());

    /* Draw a background rectangle. */

    glBindTexture(GL_TEXTURE_2D, 0);
    glColor4ub(0, 0, 0, 127);

    glEnable(GL_BLEND);
    glBegin(GL_QUADS);
    glVertex2f(gfx_width / -64.0, y+1);
    glVertex2f(gfx_width / +64.0, y+1);
    glVertex2f(gfx_width / +64.0, y-1);
    glVertex2f(gfx_width / -64.0, y-1);
    glEnd();
    glDisable(GL_BLEND);

    /* Draw the collected letter tray. */

    for (i = 0; i < worldTraySize(); ++i)
    { gfxDrawActor(worldGetTray(i), (gfx_width / -64.0) + 5.5+i, y, GL_TRUE); }

    /* Draw the player icon for the LIVES counter. */

    gfxDrawActor(player, (gfx_width / -64.0) + 1, y, GL_TRUE);

    /* Draw the lives and score text. */

    glColor3ub(0, 255, 127);

    sprintf(buffer, "x%02ld", worldGetLives());
    gfxDrawText((gfx_width / -64.0) + 2, y, buffer);

    sprintf(buffer, "$%05ld", worldGetScore());
    gfxDrawText((gfx_width / +64.0) - 6, y, buffer);

  }

}
