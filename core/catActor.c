/**
@file catActor.c

@brief Provides structures and functions for handling in-game actor instances.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdlib.h>
#include <string.h>
#include "catActor.h"
#include "catAudio.h"
#include "catWorld.h"
#include "catSystem.h"

/*
[PUBLIC] Create a new actor.
*/

actor_s *actorCreate(cat_role_t role, unsigned char flags, float x, float y)
{

  actor_s *actor = NULL;
  v2d_chain_s *chain = NULL;
  v2d_link_s *prev = NULL;

  /* Actors can only be created if there is a "world" to store them in. */
  chain = worldGetChain();
  if (!chain) { return NULL; }

  /* Create a basic actor data structure. */
  actor = (actor_s*)malloc(sizeof(actor_s));
  if (!actor) { return NULL; }
  memset(actor, 0, sizeof(actor_s));

  /* Initialise everything. */
  actor->role = role;
  actor->flags = flags;
  actor->pos[0] = x;
  actor->pos[1] = y;

  /* Create a linked-list structure for this actor so that it can be stored. */
  actor->link = v2dLinkCreate(actor);
  if (!actor->link) { actorDelete(actor); return NULL; }

  /* Letters/Exits must be first, so that they're drawn behind other actors. */
  prev = (role == CAT_ROLE_LETTER || role == CAT_ROLE_EXIT) ? NULL : chain->last;
  v2dLinkInsert(chain, prev, actor->link);

  /* Return the result. */
  return actor;

}

/*
[PUBLIC] Free a previously created actor from memory.
*/

actor_s *actorDelete(actor_s *actor)
{

  actor_s *next = NULL;
  if (!actor) { return NULL; }
  next = actorGetNext(actor);

  v2dLinkDelete(actor->link);
  free(actor);
  return next;

}

/**
@brief [INTERNAL] about

writeme

@param actor about
@param x about
@param y about
@return writeme
*/

static int actorCollide(actor_s *actor, float x, float y)
{
  if (!actor) { return 0; }
  return (actor->pos[0] + 0.5 > x - 0.5 && actor->pos[0] - 0.5 < x + 0.5 &&
          actor->pos[1] + 0.5 > y - 0.5 && actor->pos[1] - 0.5 < y + 0.5);
}

/**
@brief [INTERNAL] about

writeme

@param actor about
@return about
*/

static actor_s *actorUpdatePhysics(actor_s *actor)
{

  int axis, pass;

  if (!actor) { return NULL; }

  /* Apply gravity/friction effects to all actors (except bullets). */

  if (actor->role != CAT_ROLE_BULLET)
  {
    actor->vel[0] *= 0.500;
    actor->vel[1] += 0.005;
    if (actor->vel[1] >= 0.9) { actor->vel[1] = 0.9; }
  }

  /* Assume that an actor cannot jump by default; they must be on a surface! */

  if (actor->role != CAT_ROLE_LETTER)
  {
    if (actor->flags & CAT_FLAG_CANJUMP) { actor->flags -= CAT_FLAG_CANJUMP; }
  }

  /* Update the actor position and perform collision detection. */

  for (axis = 0; axis < 2; ++axis)
  {

    actor_s *other;

    actor->pos[axis] += actor->vel[axis];

    /* Check for collisions with other actors (Not required for most roles). */

    if (actor->role == CAT_ROLE_PLAYER || actor->role == CAT_ROLE_ENEMY)
    {

      for (other = worldGetActor(); other; other = actorGetNext(other))
      {

        if (actor != other && actorCollide(actor, other->pos[0], other->pos[1]))
        {

          if (actor->role == CAT_ROLE_PLAYER)
          {

            switch (other->role)
            {

              case CAT_ROLE_ENEMY:

              break;

              case CAT_ROLE_EXIT:
                if (sysGetInput(SYS_INPUT_GRAB) > 0 && !(actor->flags & CAT_FLAG_GRABHELD))
                {

                  actor->flags |= CAT_FLAG_GRABHELD;

                  if (other->flags & CAT_FLAG_CANOPEN)
                  {
                    if (worldInit(NULL) == 0) { return NULL; }
                  }
                  else
                  {
                    sndPlayEffect(CAT_SOUND_OPEN);
                  }

                }
              break;

              case CAT_ROLE_LETTER:
                if (sysGetInput(SYS_INPUT_GRAB) > 0 && !(actor->flags & CAT_FLAG_GRABHELD))
                {
                  actor->flags |= CAT_FLAG_GRABHELD;
                  worldCollect(other);
                }
              break;

              case CAT_ROLE_PLAYER: case CAT_ROLE_BULLET: default: break;

            }

          }
          else if (other->role == CAT_ROLE_BULLET)
          {
            other = actorDelete(other);
            actor->flags |= CAT_FLAG_DAMAGED;
          }

        }

      }

    }

    /* Check for collisions with the tiles that make up the world. */

    for (pass = 0; pass < 4; ++pass)
    {

      int x = (int)(actor->pos[0] + (pass % 2 ? 0 : 1));
      int y = (int)(actor->pos[1] + (pass / 2 ? 0 : 1));

      if (worldGetTile(x, y) && actorCollide(actor, x, y))
      {

        if (actor->role == CAT_ROLE_BULLET) { return actorDelete(actor); }

        if (axis == 1 && actor->role != CAT_ROLE_LETTER && actor->vel[1] >= 0)
        { actor->flags |= CAT_FLAG_CANJUMP; }

        if (axis == 0 && actor->role == CAT_ROLE_ENEMY)
        {
          if (actor->flags & CAT_FLAG_MIRROR) { actor->flags -= CAT_FLAG_MIRROR; }
          else                                { actor->flags |= CAT_FLAG_MIRROR; }
        }

        actor->pos[axis] = (axis ? y : x) + (actor->vel[axis] > 0 ? -1: 1);
        actor->vel[axis] = 0;
        break;

      }

    }

  }

  /* Wrap the actor position so that it starys within the stage boundaries. */

  while (actor->pos[0] <                  - 0.5) { actor->pos[0] += worldGetWidth();  }
  while (actor->pos[1] <                  - 0.5) { actor->pos[1] += worldGetHeight(); }
  while (actor->pos[0] > worldGetWidth()  - 0.5) { actor->pos[0] -= worldGetWidth();  }
  while (actor->pos[1] > worldGetHeight() - 0.5) { actor->pos[1] -= worldGetHeight(); }

  /* Store the state of the GRAB button. */

  if (actor->role == CAT_ROLE_PLAYER)
  {
    if      (sysGetInput(SYS_INPUT_GRAB) > 0 ) { actor->flags |= CAT_FLAG_GRABHELD; }
    else if (actor->flags & CAT_FLAG_GRABHELD) { actor->flags -= CAT_FLAG_GRABHELD; }
  }

  /* Return the next actor in the linked list. */

  return actorGetNext(actor);

}

/*
[PUBLIC] Update the state of an actor, based on their assigned role.
*/

actor_s *actorUpdate(actor_s *actor)
{

  if (!actor) { return NULL; }

  switch (actor->role)
  {

    /* Players are the most complex; they need to respond to player input. */

    case CAT_ROLE_PLAYER:

      /* Update the animation state. */

      if (actor->flags & CAT_FLAG_DAMAGED)
      {
        actor->timer -= 1;
        if (actor->timer <= 0)
        {
          actor->timer = 0;
          actor->flags -= CAT_FLAG_DAMAGED;
        }
      }
      else
      {
        actor->timer += 0.1;
        if (actor->timer >= 2.0) { actor->timer -= 2.0; }
      }

      /* Move left and right. */

      actor->vel[0] += (sysGetInput(SYS_INPUT_RIGHT) - sysGetInput(SYS_INPUT_LEFT)) * 0.1;

      if (sysGetInput(SYS_INPUT_RIGHT) > 0)
      {
        if (actor->flags & CAT_FLAG_MIRROR) { actor->flags -= CAT_FLAG_MIRROR; }
      }
      else if (sysGetInput(SYS_INPUT_LEFT) > 0)
      {
        actor->flags |= CAT_FLAG_MIRROR;
      }
      else if (!(actor->flags & CAT_FLAG_DAMAGED))
      {
        actor->timer = 0;
      }

      /* Jump (if on a surface). */

      if (sysGetInput(SYS_INPUT_JUMP) > 0)
      {
        if (!(actor->flags & CAT_FLAG_JUMPHELD))
        {
          actor->flags |= CAT_FLAG_JUMPHELD;
          if (actor->flags & CAT_FLAG_CANJUMP)
          {
            sndPlayEffect(CAT_SOUND_JUMP);
            actor->vel[1] = -0.18;
          }
        }
      }
      else if (actor->flags & CAT_FLAG_JUMPHELD) { actor->flags -= CAT_FLAG_JUMPHELD; }

      /* Fire (if the shoot powerup has been collected). */

      if (sysGetInput(SYS_INPUT_FIRE) > 0)
      {
        if (!(actor->flags & CAT_FLAG_FIREHELD))
        {
          actor->flags |= CAT_FLAG_FIREHELD;
          if (actor->flags & CAT_FLAG_CANFIRE)
          {
            sndPlayEffect(CAT_SOUND_FIRE);
            actorCreate(CAT_ROLE_BULLET, actor->flags, actor->pos[0], actor->pos[1]);
          }
          else { sndPlayEffect(CAT_SOUND_MEOW); }
        }
      }
      else if (actor->flags & CAT_FLAG_FIREHELD) { actor->flags -= CAT_FLAG_FIREHELD; }

      /* Drop the most-recently-collected tile. */

      if (sysGetInput(SYS_INPUT_DROP) > 0)
      {
        if (!(actor->flags & CAT_FLAG_DROPHELD))
        {
          actor->flags |= CAT_FLAG_DROPHELD;
          if (worldDropTile() == 0) { sndPlayEffect(CAT_SOUND_DROP); }
        }
      }
      else if (actor->flags & CAT_FLAG_DROPHELD) { actor->flags -= CAT_FLAG_DROPHELD; }

    break;

    /* Enemies simply need to walk forwards, based on their current facing. */

    case CAT_ROLE_ENEMY:

      if (actor->flags & CAT_FLAG_DAMAGED)
      {
        /* TODO: Perhaps they should fall off the screen? */
      }
      else
      {

        actor->vel[0] += 0.03 * ((actor->flags & CAT_FLAG_MIRROR) ? -1 : 1);

        actor->timer += 0.1;
        if (actor->timer >= 4.0) { actor->timer -= 4.0; }

      }

    break;

    /* Bullets need to move forwards. NOTE: They're not affected by physics! */

    case CAT_ROLE_BULLET:

      actor->vel[0] = 0.3 * ((actor->flags & CAT_FLAG_MIRROR) ? -1 : 1);

      actor->timer += 0.15;
      if (actor->timer >= 4.0) { actor->timer -= 4.0; }

    break;

    /* The exit door just needs to play the "open" animation when required. */

    case CAT_ROLE_EXIT:

      if (actor->flags & CAT_FLAG_CANOPEN)
      {
        actor->timer += 0.2;
        if (actor->timer >= 4.0) { actor->timer = 4.0; }
      }

    break;

    /* Any other actor types don't need to do anything. */

    case CAT_ROLE_LETTER: default: break;

  }

  /* Update the actor physics/collision. */

  return actorUpdatePhysics(actor);

}
