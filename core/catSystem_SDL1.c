/**
@file catSystem_SDL1.c

@brief This file implements the function defined in catSystem.h using SDL v1.2

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "catAudio.h"
#include "catGraphics.h"
#include "catPhrase.h"
#include "catSystem.h"
#include "catWorld.h"

/* Constants. */

#define CAT_TICK_DELAY 16 /**< @brief Delay (in milliseconds) between ticks. */

#define SYS_JOY_DEADZONE 0.25f  /**< @brief Joypad deadzone from 0.0 to 1.0. */

/* Global objects and variables. */

SDL_Joystick *sys_joypad = NULL;  /**< @brief The current joypad. */

float sys_inputs[SYS_NUM_INPUTS]; /**< @brief about */

/*
[PUBLIC] Initialise the engine and subsystems.
*/

int sysInit(int argc, char **argv)
{

  int gfx_w = 640;
  int gfx_h = 480;

  if (argc || argv) { /* Silence some "unused parameter" warnings. */ }

  /* Initialise SDL (Note: SDL_INIT_EVENTTHREAD crashes on Windows). */

  #ifdef _WIN32
  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  #else
  if (SDL_Init(SDL_INIT_EVERYTHING | SDL_INIT_EVENTTHREAD) != 0)
  #endif
  {
    fprintf(stderr, "[ERROR] Could not initialise SDL.\n");
    return 1;
  }

  /* Create a window. */

  SDL_WM_SetCaption("Initialising graphics...", NULL);

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE,   8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,  8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, SDL_TRUE);
  SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, SDL_TRUE);

  if (!SDL_SetVideoMode(gfx_w, gfx_h, 32, SDL_HWSURFACE | SDL_OPENGL))
  {
    fprintf(stderr, "[ERROR] Could not create window.\n");
    return 2;
  }

  if (gfxInit(gfx_w, gfx_h) != 0)
  {
    fprintf(stderr, "[ERROR] Could not initialise graphics subsystem.\n");
    return 3;
  }

  /* Initialise joypad. */

  SDL_WM_SetCaption("Initialising joypad...", NULL);

  sys_joypad = SDL_JoystickOpen(0);

  /* Initialise audio subsystem. If this fails, continue anyway. */

  SDL_WM_SetCaption("Initialising audio...", NULL);

  if (sndInit() != 0)
  { fprintf(stderr, "[WARNING] Couldn't initialise audio subsystem.\n"); }

  /* Load the list of known words. */

  SDL_WM_SetCaption("Loading word list (may take some time)...", NULL);

  if (phraseInit("wordlist.tsv") != 0)
  {
    fprintf(stderr, "[ERROR] Could not load phrase list file.\n");
    return 4;
  }

  /* Load a game map. TODO: Allow an argv value to override the default map? */

  SDL_WM_SetCaption("Loading map...", NULL);

  if (worldInit(NULL) != 0)
  { fprintf(stderr, "[WARNING] Could not load map.\n"); }

  /* Success. */

  SDL_WM_SetCaption("AlphaBETA Cat", NULL);

  return 0;

}

/*
[PUBLIC] Shut down the engine, and free any currently allocated memory.
*/

void sysQuit(void)
{

  worldQuit();

  phraseQuit();

  sndQuit();

  SDL_JoystickClose(sys_joypad); sys_joypad = NULL;

  gfxQuit();

  SDL_Quit();

}

/**
@brief [INTERNAL] Handle system events.

@return SDL_TRUE to keep running the program, or SDL_FALSE to terminate.
*/

static SDL_bool sysPollEvents(void)
{

  SDL_Event msg;

  while (SDL_PollEvent(&msg))
  {
    if (msg.type == SDL_QUIT) { return SDL_FALSE; }
  }

  return SDL_TRUE;

}

/**
@brief [INTERNAL] Handle user input.

@return SDL_TRUE to keep running the program, or SDL_FALSE to terminate.
*/

static SDL_bool sysUpdateInput(void)
{

  Uint8 *key = SDL_GetKeyState(NULL);
  sys_input_t i = 0;

  if (!key && !sys_joypad) { return SDL_FALSE; }

  for (i = 0; i < SYS_NUM_INPUTS; i++) { sys_inputs[i] = 0.0; }

  if (key)
  {
    if (key[SDLK_ESCAPE]) { return SDL_FALSE; }
    sys_inputs[SYS_INPUT_LEFT]  = key[SDLK_LEFT];
    sys_inputs[SYS_INPUT_RIGHT] = key[SDLK_RIGHT];
    sys_inputs[SYS_INPUT_JUMP]  = key[SDLK_UP];
    sys_inputs[SYS_INPUT_FIRE]  = key[SDLK_SPACE];
    sys_inputs[SYS_INPUT_GRAB]  = key[SDLK_DOWN];
    sys_inputs[SYS_INPUT_DROP]  = key[SDLK_TAB];
  }

  if (sys_joypad)
  {
    float tmp = SDL_JoystickGetAxis(sys_joypad, 0) / 32768.0;
    if (tmp < -SYS_JOY_DEADZONE || tmp > SYS_JOY_DEADZONE)
    {
      if (sys_inputs[SYS_INPUT_LEFT]  < -tmp) { sys_inputs[SYS_INPUT_LEFT]  = -tmp; }
      if (sys_inputs[SYS_INPUT_RIGHT] < +tmp) { sys_inputs[SYS_INPUT_RIGHT] = +tmp; }
    }

    if (SDL_JoystickGetButton(sys_joypad, 0)) { sys_inputs[SYS_INPUT_JUMP] = 1; }
    if (SDL_JoystickGetButton(sys_joypad, 1)) { sys_inputs[SYS_INPUT_FIRE] = 1; }
    if (SDL_JoystickGetButton(sys_joypad, 2)) { sys_inputs[SYS_INPUT_GRAB] = 1; }
    if (SDL_JoystickGetButton(sys_joypad, 3)) { sys_inputs[SYS_INPUT_DROP] = 1; }

  }

  return SDL_TRUE;

}

/*
[PUBLIC] This function handles the main progam loop.
*/

void sysMainLoop(void)
{

  SDL_bool updated = SDL_FALSE;
  Uint32   timer   = 0;

  /* Main program loop. */

  timer = SDL_GetTicks() + CAT_TICK_DELAY;

  while (SDL_TRUE)
  {

    /* Processing loop. */

    updated = SDL_FALSE;

    while (timer < SDL_GetTicks())
    {

      if (!sysPollEvents()) { return; }
      if (!sysUpdateInput()) { return; }

      worldUpdate();

      /* Handle program timing. */

      timer += CAT_TICK_DELAY;
      updated = SDL_TRUE;

    }

    /* Determine whether to update the display or not. */

    if (updated)
    {
      gfxDrawGame();
      SDL_GL_SwapBuffers();
    }
    else if (!sysPollEvents()) { return; }

    /* Thread sleep. */

    SDL_Delay(1);

  }

}

/*
[PUBLIC] Get the state of a specific input slot.
*/

float sysGetInput(sys_input_t slot)
{ return slot < SYS_NUM_INPUTS ? sys_inputs[slot] : 0; }
