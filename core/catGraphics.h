/**
@file catGraphics.h

@brief Handles graphics.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CAT_GRAPHICS_H__
#define __CAT_GRAPHICS_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise the graphics sub-system.

Call this before doing anything else.

@param w The width of the display area.
@param h The height of the display area.
@return Zero on success, or non-zero on failure.
*/

int gfxInit(int w, int h);

/**
@brief Shut down the graphics sub-system and free any allocated memory.

Call this when the program ends.
*/

void gfxQuit(void);

/**
@brief Prepare any per-level graphical stuff when a new map is loaded.

This is called internally by worldInit().

@param backdrop The image file to use as a background image.
*/

void gfxPrepareLevel(const char *backdrop);

/**
@brief Clean up any per-level graphical stuff.

This is called automatically by gfxPrepareLevel() and gfxQuit().
*/

void gfxCleanupLevel(void);

/**
@brief Draw everything in the game world.

Does what it say on the tin.
*/

void gfxDrawGame(void);

#ifdef __cplusplus
}
#endif

#endif /* __CAT_GRAPHICS_H__ */
