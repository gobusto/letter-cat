/**
@file catPhrase.c

@brief Provides structures and functions for handling the "word list" lookup.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "catPhrase.h"
#include "bits/wordtree.h"
#include "bits/misc.h"

/* Global objects and variables. */

wordtree_s *cat_word_list = NULL; /**< @brief about */

/*
[PUBLIC] about
*/

int phraseInit(const char *file_name)
{

  FILE *src = NULL;
  char *line = NULL;

  phraseQuit();

  /* Open the word list file. */

  if (!file_name) { return -1; }
  src = fopen(file_name, "rb");
  if (!src) { return -2; }

  /* Allocate a dictionary structure for storing the word list. */

  cat_word_list = treeCreate();
  if (!cat_word_list) { fclose(src); return -3; }

  while ((line = cat_readline(src)))
  {

    phrase_s *phrase = NULL;
    char *tmp = NULL;
    void *old = NULL;

    /* Look for the first tab separator. */

    if ((tmp = strstr(line, "\t")))
    {

      if ((phrase = (phrase_s*)malloc(sizeof(phrase_s))))
      {

        *tmp = 0;

        /* Add the new phrase structure to the word list dictionary. */

        /* fprintf(stderr, "[DEBUG] New key: '%s'\n", line); */

        if (treeInsert(cat_word_list, line, phrase, &old))
        {

          /* If the word already existed in the dictionary, free the old data. */

          if (old)
          {
            fprintf(stderr, "[INFO] Freeing old data for key '%s'\n", line);
            free(old);
          }

          memset(phrase, 0, sizeof(phrase_s));

          phrase->score = atof(tmp+1);

          if ((tmp = strstr(tmp+1, "\t")))
          {

            phrase->lives = atof(tmp);

            if ((tmp = strstr(tmp+1, "\t")))
            {

              tmp++;

              if      (strcmp(tmp, "SHOOT"  ) == 0) { phrase->flags |= PHRASE_FLAG_SHOOT; }
              else if (strcmp(tmp, "ENEMY"  ) == 0) { phrase->flags |= PHRASE_FLAG_ENEMY; }
              else if (strcmp(tmp, "KILLALL") == 0) { phrase->flags |= PHRASE_FLAG_KILLALL; }

            } else { fprintf(stderr, "[WARNING] 'flags' missing: %s\n", line); }

          } else { fprintf(stderr, "[WARNING] 'lives' missing: %s\n", line); }

        }
        else
        {
          fprintf(stderr, "[WARNING] Could not store key: '%s'\n", line);
          free(phrase);
        }

      } else { fprintf(stderr, "[WARNING] malloc() failed: '%s'\n", line); }

    } else { fprintf(stderr, "[WARNING] Malformed line: '%s'\n", line); }

    /* Free the temporary line buffer. */

    free(line);

  }

  /* Close the input file and return the result. */

  fclose(src);
  return 0;

}

/**
@brief [INTERNAL] Called for each item in the word tree during deletion.

This is essentially just a complicated wrapper aroundthe "free" function.

@param name The "key" used to find this data item.
@param data The phrase_s structure stored for this key within the tree.
@param user An optional "user data" pointer; not used by this program.
@return Always NULL. It doesn't really matter, as it's being deleted anyway...
*/

static void *phraseQuit_callback(const char *name, void *data, void *user)
{
  if (name || user) { /* Silence "unused parameter" warnings. */ }
  free(data);
  return NULL;
}

/*
[PUBLIC] about
*/

void phraseQuit(void)
{ cat_word_list = treeDelete(cat_word_list, phraseQuit_callback, NULL); }

/*
[PUBLIC] about
*/

const phrase_s *phraseFind(const char *text)
{ return treeSearch(cat_word_list, text); }
