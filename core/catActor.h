/**
@file catActor.h

@brief Provides structures and functions for handling in-game actor instances.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CAT_ACTOR_H__
#define __CAT_ACTOR_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bits/chain.h"

/* Actor flags (NOTE: These do not apply to "letter" tile actors). */

#define CAT_FLAG_MIRROR   0x01  /**< @brief Reverse sprite so it faces left. */
#define CAT_FLAG_DAMAGED  0x02  /**< @brief The actor has just taken damage. */
#define CAT_FLAG_CANJUMP  0x04  /**< @brief The actor is resting on a floor. */
#define CAT_FLAG_CANFIRE  0x08  /**< @brief The actor is able to fire stuff. */

#define CAT_FLAG_JUMPHELD 0x10  /**< @brief PLAYER ONLY: Jump input is held. */
#define CAT_FLAG_FIREHELD 0x20  /**< @brief PLAYER ONLY: Fire input is held. */
#define CAT_FLAG_DROPHELD 0x40  /**< @brief PLAYER ONLY: Drop input is held. */
#define CAT_FLAG_GRABHELD 0x80  /**< @brief PLAYER ONLY: Grab input is held. */

#define CAT_FLAG_CANOPEN  0x10  /**< @brief EXIT ONLY: The door should open. */

/**
@brief Get the next actor structure in a linked-list of actor structures.

This macro is provided for convenience, in order to avoid casting everywhere...

@param a The current actor in the linked-list.
@return The next actor in the linked-list, or NULL if there isn't one.
*/

#define actorGetNext(a) ((actor_s*)v2dLinkGetNext((a) ? (a)->link : 0))

/**
@brief Enumerates possible types of actor within the world.

All interactive objects in the game must play one of these roles.
*/

typedef enum
{

  CAT_ROLE_PLAYER,  /**< @brief A player-controlled character. */
  CAT_ROLE_BULLET,  /**< @brief A bullet fired by a player.    */
  CAT_ROLE_LETTER,  /**< @brief A collectable letter tile.     */
  CAT_ROLE_ENEMY,   /**< @brief An enemy character.            */
  CAT_ROLE_EXIT     /**< @brief The level exit.                */

} cat_role_t;

/**
@brief A generic "actor" structure, used to represent in-game objects.

Actors are used to represent anything interactive in the game world.
*/

typedef struct
{

  cat_role_t role;      /**< @brief Specifies the basic actor behaviour. */
  unsigned char flags;  /**< @brief Flags OR a Latin-1 letter. */

  float timer;  /**< @brief Used to regulate animation timing, etc. */

  float pos[2]; /**< @brief Actor position. */
  float vel[2]; /**< @brief Actor velocity. */

  v2d_link_s *link; /**< @brief Linked list data. */

} actor_s;

/**
@brief Create a new actor.

@param role The type of actor to be created.
@param flags Flags OR a Latin-1 letter.
@param x The initial X position of the actor.
@param y The initial Y position of the actor.
@return A new actor structure on success, or NULL on failure.
*/

actor_s *actorCreate(cat_role_t role, unsigned char flags, float x, float y);

/**
@brief Delete a previously-created actor structure.

@param actor The actor to be deleted.
@return The next actor in the list on success, or NULL on failure.
*/

actor_s *actorDelete(actor_s *actor);

/**
@brief Update the current AI-state of an actor.

This is called once-per-game-loop by the worldUpdate() function.

@param actor The actor to be updated.
@return The next actor in the chain.
*/

actor_s *actorUpdate(actor_s *actor);

#ifdef __cplusplus
}
#endif

#endif /* __CAT_ACTOR_H__ */
