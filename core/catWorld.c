/**
@file catWorld.c

@brief Handles the currently-active map.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "catAudio.h"
#include "catGraphics.h"
#include "catPhrase.h"
#include "catWorld.h"
#include "bits/latin1.h"
#include "bits/misc.h"

/**
@brief [INTERNAL] about

writeme
*/

typedef struct
{

  long lives; /**< @brief about */
  long score; /**< @brief about */

  char *target_word;  /**< @brief The word required to open the exit door. */
  char *next_map;     /**< @brief The next map file to use when completed. */

  long current_size;  /**< @brief The current maximum allowed word length. */
  long maximum_size;  /**< @brief Equivalent to strlen(world->target_word) */
  actor_s **collected;  /**< @brief Pointers to "collected" letter tiles. */

  int width;  /**< @brief about */
  int height; /**< @brief about */
  int **tile; /**< @brief about */

  v2d_chain_s *actor; /**< @brief about */

} world_s;

/* Global objects and variables. */

world_s *cat_world = NULL;  /**< @brief about */

/**
@brief [INTERNAL] about

writeme

@param line about
@param w about
@param h about
@return writeme
*/

static char *worldReadHeader(const char *line, int *w, int *h)
{

  char *target = NULL;

  if (!line) { return NULL; }
  if (strncmp(line, "CATMAP|", 7) != 0) { return NULL; }
  line += 7;

  if (w)
  {
    *w = atoi(line);
    #ifdef NDEBUG
      fprintf(stderr, "[DEBUG] Map width is %d\n", *w);
    #endif
    if (*w < 1) { return NULL; }
  }

  line = strchr(line, '|');
  if (!line) { return NULL; }
  ++line;

  if (h)
  {
    *h = atoi(line);
    #ifdef NDEBUG
      fprintf(stderr, "[DEBUG] Map height is %d\n", *h);
    #endif
    if (*h < 1) { return NULL; }
  }

  line = strchr(line, '|');
  if (!line) { return NULL; }
  ++line;

  target = (char*)malloc(strlen(line) + 1);
  if (!target) { return NULL; }
  strcpy(target, line);

  return target;

}

/*
[PUBLIC] Load a new world from a file.
*/

int worldInit(const char *file_name)
{

  FILE *map = NULL;
  char *line = NULL;
  char *target = NULL;
  long score, lives;
  int status, x, y;

  /* Open the input file. Use default values if no filename is specified. */

  if (!file_name)
  {
    if (cat_world ) { file_name = cat_world->next_map; }
    if (!file_name) { file_name = "maps/start.txt";    }
  }

  map = fopen(file_name, "rb");

  if (!map)
  {
    fprintf(stderr, "[WARNING] Could not load map: %s\n", file_name);
    return -1;
  }

  fprintf(stderr, "[INFO] Loading map: %s\n", file_name);

  /* Read the "header" line to get the map width/height and target word. */

  line = cat_readline(map);
  target = worldReadHeader(line, &x, &y);
  if (line) { free(line); }

  if      (!target           ) {               fclose(map); return -1; }
  else if (strlen(target) < 3) { free(target); fclose(map); return -1; }

  /* Free any old map data, but remember the current score and lives values. */

  lives = worldGetLives();
  score = worldGetScore();

  worldQuit();

  /* Allocate a new world structure. */

  cat_world = (world_s*)malloc(sizeof(world_s));
  if (!cat_world) { free(target); fclose(map); return -1; }
  memset(cat_world, 0, sizeof(world_s));

  cat_world->lives = lives;
  cat_world->score = score;
  cat_world->target_word = target;

  cat_world->current_size = 3;
  cat_world->maximum_size = strlen(cat_world->target_word);

  /* Init "status" variable. If this is non-zero later, something's wrong! */

  status = 0;

  /* Allocate memory to store a pointer to each of the "collected" letters. */

  cat_world->collected = (actor_s**)malloc(sizeof(actor_s*) * cat_world->maximum_size);

  if (cat_world->collected)
  { memset(cat_world->collected, 0, sizeof(actor_s*) * cat_world->maximum_size); }
  else
  { status = 1; }

  /* Allocate memory for tiles. */

  cat_world->width  = x;
  cat_world->height = y;
  cat_world->tile = (int**)malloc(sizeof(int*) * cat_world->width);

  if (cat_world->tile)
  {
    for (x = 0; x < cat_world->width; x++)
    {
      cat_world->tile[x] = (int*)malloc(sizeof(int) * cat_world->height);
      if (cat_world->tile[x])
      { memset(cat_world->tile[x], 0, sizeof(int) * cat_world->height); }
      else { status = 1; }
    }
  } else { status = 1; }

  /* Allocate memory for actors. */

  cat_world->actor = v2dChainCreate(0, NULL);
  if (!cat_world->actor) { status = 1; }

  /* Verify that everything has been allocated successfully. */

  if (status != 0)
  {
    fclose(map);
    worldQuit();
    return -1;
  }

  /* Load the tiles and actors. */

  for (y = 0; y < cat_world->height; y++)
  {

    line = cat_readline(map);
    if (!line) { break; }

    for (x = 0; x < cat_world->width; x++)
    {

      /* For Latin-1 to work, characters must be read as UNSIGNED bytes. */
      unsigned char c = (unsigned char)line[x];

      /* If a NULL terminator is found, stop reading the line here. */
      if (!c) { break; }
      /* WALLS: 123456789()[]{} represent EGA wall colors (zero is ignored). */
      else if (c >= '0' && c <= '9') { cat_world->tile[x][y] = c - '0'; }
      else if (c == '(') { cat_world->tile[x][y] = 10; }
      else if (c == ')') { cat_world->tile[x][y] = 11; }
      else if (c == '[') { cat_world->tile[x][y] = 12; }
      else if (c == ']') { cat_world->tile[x][y] = 13; }
      else if (c == '{') { cat_world->tile[x][y] = 14; }
      else if (c == '}') { cat_world->tile[x][y] = 15; }
      /* Upper/lower case ASCII/Latin-1 characters are treated as letters. */
      else if (latin1_isalpha(c)) { actorCreate(CAT_ROLE_LETTER, c, x, y); }
      /* Punctuation characters represent various actor types. */
      else if (c == '&') { actorCreate(CAT_ROLE_PLAYER, 0, x, y); }
      else if (c == '@') { actorCreate(CAT_ROLE_EXIT, 0, x, y); }
      else if (c == '>') { actorCreate(CAT_ROLE_ENEMY, 0, x, y); }
      else if (c == '<') { actorCreate(CAT_ROLE_ENEMY, CAT_FLAG_MIRROR, x, y); }

    }

    free(line);

  }

  /* Load media. */

  if ((line = cat_readline(map))) { gfxPrepareLevel(line); free(line); }
  if ((line = cat_readline(map))) { sndPlayMusic(line);    free(line); }

  cat_world->next_map = cat_readline(map);

  /* Close the input file and report success. */

  fclose(map);
  return 0;

}

/*
[PUBLIC] Free the currently-active map from memory.
*/

void worldQuit(void)
{

  if (!cat_world) { return; }

  /* Free the "next world" string. */

  if (cat_world->next_map) { free(cat_world->next_map); }

  /* Free tiles. */

  if (cat_world->tile)
  {

    int x = 0;

    for (x = 0; x < cat_world->width; x++)
    {
      if (cat_world->tile[x]) { free(cat_world->tile[x]); }
    }

    free(cat_world->tile);

  }

  /* Free the word buffers. */

  if (cat_world->target_word) { free(cat_world->target_word); }
  if (cat_world->collected) { free(cat_world->collected); }

  /* Free actors. */

  if (cat_world->actor)
  {
    while (cat_world->actor->first) { actorDelete((actor_s*)cat_world->actor->first->data); }
    v2dChainDelete(cat_world->actor);
  }

  /* Free the main world structure. */

  free(cat_world);
  cat_world = NULL;

}

/*
[PUBLIC] Update the state of the world.
*/

void worldUpdate(void)
{
  actor_s *actor = NULL;
  for (actor = worldGetActor(); actor; actor = actorUpdate(actor)) { }
}

/*
[PUBLIC] about
*/

int worldGetWidth(void) { return cat_world ? cat_world->width : 0; }

/*
[PUBLIC] about
*/

int worldGetHeight(void) { return cat_world ? cat_world->height : 0; }

/*
[PUBLIC] about
*/

int worldGetTile(int x, int y)
{

  if (!cat_world) { return 0; }

  while (x < 0) { x += cat_world->width;  }
  while (y < 0) { y += cat_world->height; }

  return cat_world->tile[x % cat_world->width][y % cat_world->height];

}

/*
[PUBLIC] Get a pointer to the "chain" structure used to store the actor list.
*/

v2d_chain_s *worldGetChain(void) { return cat_world ? cat_world->actor : NULL; }

/*
[PUBLIC] about
*/

long worldWordSize(void) { return cat_world ? cat_world->maximum_size : 0; }

/*
[PUBLIC] about
*/

long worldTraySize(void) { return cat_world ? cat_world->current_size : 0; }

/*
[PUBLIC] about
*/

const char *worldGetWord(void) { return cat_world ? cat_world->target_word : NULL; }

/*
[PUBLIC] about
*/

actor_s *worldGetTray(long slot)
{
  if (!cat_world || slot < 0) { return NULL; }
  if (slot >= cat_world->maximum_size) { return NULL; }
  return cat_world->collected[slot];
}

/*
[PUBLIC] about
*/

int worldCollect(actor_s *actor)
{

  char *text = NULL;
  long slot = 0;
  cat_sound_t sound;

  if (!cat_world || !actor) { return -1; }

  /* Mark this actor as "collected" if they haven't been grabbed already. */

  if (actor->timer > 0) { return -1; } else { actor->timer = 1; }

  /* Add the actor to the "tray" of letter tiles. */

  if (cat_world->collected[cat_world->current_size-1])
  {

    /* If the tray is full, slide everything back by one place. */

    cat_world->collected[0]->timer = 0;

    for (slot = 1; slot < cat_world->current_size; ++slot)
    { cat_world->collected[slot-1] = cat_world->collected[slot]; }

    cat_world->collected[cat_world->current_size-1] = actor;

  }
  else
  {

    /* If the tray is NOT full, put this letter in the next free slot. */

    for (slot = 0; slot < cat_world->current_size; ++slot)
    {
      if (!cat_world->collected[slot])
      {
        cat_world->collected[slot] = actor;
        break;
      }
    }

  }

  /* Check if the new collection of letters spells anything interesting... */

  sound = CAT_SOUND_GRAB;

  if (cat_world->collected[cat_world->current_size-1])
  {

    const phrase_s *phrase = NULL;
    long multiplier = 0;

    /* Convert the list of actor tiles into a standard C string. */

    text = (char*)malloc(cat_world->current_size+1);
    if (!text) { return -1; }
    memset(text, 0, cat_world->current_size+1);

    /* Upper-case letters double the score of any words made. */

    for (multiplier = 1, slot = 0; slot < cat_world->current_size; ++slot)
    {
      if (latin1_isupper(cat_world->collected[slot]->flags)) { multiplier *= 2; }
      text[slot] = latin1_toupper(cat_world->collected[slot]->flags);
    }

    /* Look up the alleged word in the dictionary. */

    phrase = phraseFind(text);

    if (phrase)
    {

      while (worldDropTile() == 0) { /* Drop all tiles. */ }

      /* Increase carrying capacity by one tile. */

      if (cat_world->current_size < cat_world->maximum_size)
      {
        cat_world->collected[cat_world->current_size] = NULL;
        ++cat_world->current_size;
      }

      /* Update Score/Lives and apply any special effects. */

      cat_world->lives += phrase->lives;
      cat_world->score += phrase->score * multiplier;

      sound = CAT_SOUND_MEOW; /* <-- TODO: Use a special sound effect here. */

      if (phrase->flags & PHRASE_FLAG_SHOOT)
      {
        actor_s *player = NULL;
        for (player = worldGetActor(); player; player = actorGetNext(player))
        {
          if (player->role == CAT_ROLE_PLAYER && !(player->flags & CAT_FLAG_CANFIRE))
          { player->flags |= CAT_FLAG_CANFIRE; sound = CAT_SOUND_BONUS; }
        }
      }

      if (phrase->flags & PHRASE_FLAG_ENEMY)
      {
        /* TODO: Spawn a new enemy. */
      }

      if (phrase->flags & PHRASE_FLAG_KILLALL)
      {
        actor_s *enemy = NULL;
        for (enemy = worldGetActor(); enemy; enemy = actorGetNext(enemy))
        {
          if (enemy->role == CAT_ROLE_ENEMY && !(enemy->flags & CAT_FLAG_DAMAGED))
          { enemy->flags |= CAT_FLAG_DAMAGED; sound = CAT_SOUND_BONUS; }
        }
      }

    }

    /* Check if this is the target word. */

    if (strcmp(text, cat_world->target_word) == 0)
    {
      actor_s *door = NULL;
      for (door = worldGetActor(); door; door = actorGetNext(door))
      {
        if (door->role == CAT_ROLE_EXIT && !(door->flags & CAT_FLAG_CANOPEN))
        { door->flags |= CAT_FLAG_CANOPEN; sound = CAT_SOUND_OPEN; }
      }
    }

    /* Free the temporary word buffer again. */

    free(text);

  }

  /* Report success. */

  sndPlayEffect(sound);
  return 0;

}

/*
[PUBLIC] about
*/

int worldDropTile(void)
{

  long slot = 0;

  if (!cat_world) { return -1; }

  for (slot = cat_world->current_size; slot > 0; --slot)
  {
    if (cat_world->collected[slot-1])
    {
      cat_world->collected[slot-1]->timer = 0;
      cat_world->collected[slot-1] = NULL;
      return 0;
    }
  }

  return 1;

}

/*
[PUBLIC] about
*/

long worldGetLives(void) { return cat_world ? cat_world->lives : 3; }

/*
[PUBLIC] about
*/

long worldGetScore(void) { return cat_world ? cat_world->score : 0; }
