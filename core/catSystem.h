/**
@file catSystem.h

@brief Handles high-level engine functionality, such a start-up and shut-down.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CAT_SYSTEM_H__
#define __CAT_SYSTEM_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Defines possible inputs.

These are used to query input devices in an abstract way, via sysGetInput().
*/

typedef enum
{

  SYS_INPUT_LEFT,   /**< @brief Move the player character to the left.  */
  SYS_INPUT_RIGHT,  /**< @brief Move the player character to the right. */
  SYS_INPUT_JUMP,   /**< @brief Make the player jump, if on the ground. */
  SYS_INPUT_FIRE,   /**< @brief Shoot, if the player has the right power-up. */
  SYS_INPUT_GRAB,   /**< @brief Pick up the letter behind the player sprite. */
  SYS_INPUT_DROP,   /**< @brief Drop up the last letter picked up.           */
  SYS_NUM_INPUTS    /**< @brief The total number of input "slots" available. */

} sys_input_t;

/**
@brief Initialise the engine and subsystems.

This function should be called when the program first starts.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int sysInit(int argc, char **argv);

/**
@brief Shut down the engine, and free any currently allocated memory.

This function should be called when the program terminates.
*/

void sysQuit(void);

/**
@brief This function handles the main progam loop.

Once called, this function will continue to run until a termination event is
received.
*/

void sysMainLoop(void);

/**
@brief Get the state of a specific input slot.

@todo Allow a "player" ID to be specified?

@param slot The type of input to check.
@return The state of the input slot specified.
*/

float sysGetInput(sys_input_t slot);

#ifdef __cplusplus
}
#endif

#endif /* __CAT_SYSTEM_H__ */
