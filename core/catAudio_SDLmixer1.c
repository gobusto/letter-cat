/**
@file catAudio_SDLmixer1.c

@brief This file implements catAudio.h using SDL mixer v1.2

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL/SDL_mixer.h>
#include "catAudio.h"

/* Global objects and variables. */

SDL_bool snd_initialised = SDL_FALSE; /**< @brief about */

Mix_Music *snd_music = NULL;  /**< @brief about */

Mix_Chunk *snd_jump = NULL;  /**< @brief about */
Mix_Chunk *snd_fire = NULL;  /**< @brief about */
Mix_Chunk *snd_grab = NULL;  /**< @brief about */
Mix_Chunk *snd_drop = NULL;  /**< @brief about */
Mix_Chunk *snd_open = NULL;  /**< @brief about */
Mix_Chunk *snd_meow = NULL;  /**< @brief about */
Mix_Chunk *snd_bonus = NULL;  /**< @brief about */

/*
[PUBLIC] Initialise the audio subsystem.
*/

int sndInit(void)
{

  sndQuit();

  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0) { return -1; }

  snd_jump = Mix_LoadWAV("sounds/jump.wav");
  snd_fire = Mix_LoadWAV("sounds/fire.wav");
  snd_grab = Mix_LoadWAV("sounds/grab.wav");
  snd_drop = Mix_LoadWAV("sounds/drop.wav");
  snd_open = Mix_LoadWAV("sounds/open.wav");
  snd_meow = Mix_LoadWAV("sounds/meow.wav");
  snd_bonus= Mix_LoadWAV("sounds/bonus.wav");

  snd_initialised = SDL_TRUE;
  return 0;

}

/*
[PUBLIC] Shut down the audio subsystem.
*/

void sndQuit(void)
{

  if (!snd_initialised) { return; }

  sndStopMusic();

  Mix_HaltChannel(-1);
  Mix_FreeChunk(snd_jump); snd_jump = NULL;
  Mix_FreeChunk(snd_fire); snd_fire = NULL;
  Mix_FreeChunk(snd_grab); snd_grab = NULL;
  Mix_FreeChunk(snd_drop); snd_drop = NULL;
  Mix_FreeChunk(snd_open); snd_open = NULL;
  Mix_FreeChunk(snd_meow); snd_meow = NULL;
  Mix_FreeChunk(snd_bonus); snd_bonus = NULL;

  Mix_CloseAudio();
  snd_initialised = SDL_FALSE;

}

/*
[PUBLIC] Play a sound effect.
*/

void sndPlayEffect(cat_sound_t id)
{

  if (!snd_initialised) { return; }

  switch (id)
  {
    case CAT_SOUND_JUMP: Mix_PlayChannel(-1, snd_jump, 0); break;
    case CAT_SOUND_FIRE: Mix_PlayChannel(-1, snd_fire, 0); break;
    case CAT_SOUND_GRAB: Mix_PlayChannel(-1, snd_grab, 0); break;
    case CAT_SOUND_DROP: Mix_PlayChannel(-1, snd_drop, 0); break;
    case CAT_SOUND_OPEN: Mix_PlayChannel(-1, snd_open, 0); break;
    case CAT_SOUND_MEOW: Mix_PlayChannel(-1, snd_meow, 0); break;
    case CAT_SOUND_BONUS: default: Mix_PlayChannel(-1, snd_bonus, 0); break;
  }

}

/*
[PUBLIC] Play (and loop) a music track.
*/

int sndPlayMusic(const char *file_name)
{

  if (!snd_initialised) { return 1; }

  sndStopMusic();

  if (!file_name) { return -1; }
  snd_music = Mix_LoadMUS(file_name);
  if (!snd_music) { return -2; }
  if (Mix_PlayMusic(snd_music, -1) != 0) { return -3; }

  return 0;

}

/*
[PUBLIC] Stop any currently playing music.
*/

void sndStopMusic(void)
{

  if (!snd_initialised) { return; }

  Mix_HaltMusic();
  Mix_FreeMusic(snd_music);
  snd_music = NULL;

}
