/**
@file catWorld.h

@brief Handles the currently-active map.

Copyright (C) 2014 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __CAT_WORLD_H__
#define __CAT_WORLD_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "catActor.h"

/**
@brief Get the first actor in the world linked-list.

@return The first actor in the world, or NULL if none exists.
*/

#define worldGetActor() (actor_s*)(worldGetChain() ? (worldGetChain()->first ? worldGetChain()->first->data : 0) : 0)

/**
@brief Load a new world from a file.

This automatically frees any previous level data from memory.

@param file_name The level file to be loaded.
@return Zero on success, or non-zero on failure.
*/

int worldInit(const char *file_name);

/**
@brief Free the currently-active map from memory.

This should be called when the game engine shuts down.
*/

void worldQuit(void);

/**
@brief Update the state of the world.

This is essentially a convenience function that calls actorUpdate() on every
actor in the world.
*/

void worldUpdate(void);

/**
@brief about

@return writeme
*/

int worldGetWidth(void);

/**
@brief about

@return writeme
*/

int worldGetHeight(void);

/**
@brief about

@param x about
@param y about
@return writeme
*/

int worldGetTile(int x, int y);

/**
@brief Get a pointer to the "chain" structure used to store the actor list.

@return The world chain structure, or NULL of no world currently exists.
*/

v2d_chain_s *worldGetChain(void);

/**
@brief about

@return writeme
*/

long worldWordSize(void);

/**
@brief about

@return writeme
*/

long worldTraySize(void);

/**
@brief about

@return writeme
*/

const char *worldGetWord(void);

/**
@brief about

@param slot about
@return writeme
*/

actor_s *worldGetTray(long slot);

/**
@brief about

@param actor about
@return writeme
*/

int worldCollect(actor_s *actor);

/**
@brief about

@return writeme
*/

int worldDropTile(void);

/**
@brief about

@return writeme
*/

long worldGetLives(void);

/**
@brief about

@return writeme
*/

long worldGetScore(void);

#ifdef __cplusplus
}
#endif

#endif /* __CAT_WORLD_H__ */
