/**
@mainpage catgame

@section intro_sec Introduction

A game written in C.
*/

/**
@file main.c

@brief The program start point.

This file simply contains the main() function called when the program begins.
*/

#include "core/catSystem.h"

/**
@brief The program entry point.

The program begins here.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int main(int argc, char *argv[])
{
  if (sysInit(argc, argv) == 0) { sysMainLoop(); }
  sysQuit();
  return 0;
}
