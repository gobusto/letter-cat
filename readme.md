Letter Cat
==========

This document should tell you everything you'll need to know about playing the
game.

How to Play
-----------

The aim of each level is to collect the letters to spell out the word shown at
the top of the screen. You are initially able to hold three letters; spelling
a three-letter word adds an extra slot so that you can spell four-letter words
and so on. Some words have special effects, granting extra lives or power-ups.

If you are damaged by an enemy, you will lose one of your letter slots. If you
are damaged when only two slots remain, you will lose a life and must re-start
the level (NOTE: This is currently unimplemented; enemies can't hurt you yet).

Each word is worth a certain amount of points. Every RED letter tile collected
applies a 2x multiplier to the basic word score, so that a word worth 5 points
spelled with three red letter tiles gives 5 x 2 x 2 x 2 = 40 points.


Controls
--------

### Keyboard

+ Left/Right arrow keys: Move left or right.
+ Up arrow key: Jump.
+ Space bar: Shoot (if you have the power-up).
+ Down arrow key: Grab letter/Use exit.
+ Tab key: Drop the most recent letter collected.

### Joypad

+ Axis 0 (Left stick on XBOX 360): Move left or right.
+ Fire button 0 (A on XBOX 360): Jump.
+ Fire button 1 (B on XBOX 360): Shoot (if you have the power-up).
+ Fire button 2 (X on XBOX 360): Grab letter/Use exit.
+ Fire button 3 (Y on XBOX 360): Drop the most recent letter collected.

TODO
----

+ The player can't die yet, so it's not possible to lose.
+ More levels are needed!

Licensing
---------

The source code of this program is available under the MIT/X11 license:

<http://opensource.org/licenses/MIT>

The media files (sound, images, and maps) are under CC0 instead:

<http://creativecommons.org/publicdomain/zero/1.0/>

Contact
-------

Send any comments to go?busto@gmail!com

